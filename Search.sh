#!/bin/bash
# 
#┏━┓┏━╸┏━┓┏━┓┏━╸╻ ╻
#┗━┓┣╸ ┣━┫┣┳┛┃  ┣━┫
#┗━┛┗━╸╹ ╹╹┗╸┗━╸╹ ╹
# written by Christos Angelopoulos February 2022
#
ENGINE=("" "https://duckduckgo.com/?q=" "https://www.google.com/search?sxsrf=ALeKk03Og8Dh5ln0ZgjH8rMA8ZeVraWqiA%3A1615550843347&ei=e1lLYN7UFOeRrgSfnpuIBw&q=" "https://www.youtube.com/results?search_query=" "https://odysee.com/$/search?q=" "https://www.google.com/search?sxsrf=ALeKk03Og8Dh5ln0ZgjH8rMA8ZeVraWqiA%3A1615550843347&ei=e1lLYN7UFOeRrgSfnpuIBw&q=translate " "https://piratebay.live/search/" "https://en.wikipedia.org/wiki/" "https://www.subs4free.club/search_report.php?search=" "https://www.imdb.com/find?q=" "https://www.jstor.org/action/doBasicSearch?Query=")
SELECT=$(yad --list \
		   --title="Search" \
		   --column="Engine" \
		   --column="Array" \
		    Exit 0 \
		    DuckDuckGo 1 \
		    Google 2 \
		    YouTube 3 \
		    Odysee 4 \
		    Google_Translate 5 \
		    ThePirateBay 6 \
		    WikiPedia 7 \
		    Subs4Free 8 \
		    ImDb 9 \
		    JStor 10 \
		   --print-column=2 \
		   --hide-column=2 \
		   --geometry=250x200+0+0 \
		   --window-icon=search \
		   --undecorated \
		   --skip-taskbar \
		   --no-headers \
		   --no-buttons )
case $? in 
  0)  
  ;; 
  1) exit
  ;; 
esac
	
SELECT=${SELECT/|/} 
if [ $SELECT = 0 ]
then 
 exit
fi  
echo "$SELECT"
SELECTEDURL=${ENGINE[$SELECT]}
ITEM=$(yad --entry \
							 --title="Search" \
							 --geometry=250x30+0+0 \
		   			--window-icon=search \
		   			--licon=search \
		   			--undecorated \
		   			--no-buttons 
		   			--close-on-unfocus)
case $? in 
 0)  
 ;; 
 1) exit
 ;; 
esac
if [ "$ITEM" = "" ]
then 
 exit
fi
#firefox "$SELECTEDURL""$ITEM"
brave-browser-stable "$SELECTEDURL""$ITEM"
